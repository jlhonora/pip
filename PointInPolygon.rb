#!/usr/bin/ruby

# Computes the Point In Polygon Algorithm
# Sum of angles version
# Derived mainly from http://alienryderflex.com/polygon/

def pip(test_point, polygon) 
	n_sides = polygon.length
	j = n_sides - 1
	oddNodes = false	
	n_sides.times do |i|
		point1 = polygon[i]
		point2 = polygon[j]
		if ((point1[:y] <  test_point[:y] && point2[:y] >= test_point[:y]) ||
		   (point2[:y] <  test_point[:y] && point1[:y] >= test_point[:y]) &&
		   (point1[:x] <= test_point[:x] || point2[:x] <= test_point[:x])) 
			oddNodes ^= (point1[:x] + (test_point[:y] - point1[:y])/(point2[:y] - point1[:y])*(point2[:x] - point1[:x]) < test_point[:x])
		end
		j = i	
	end
	return oddNodes
end
