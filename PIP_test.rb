#!/usr/bin/ruby
# Tests the Point In Polygon algorith library

require "PointInPolygon"

test_number = 2

case test_number
when 1
	# Convex polygon
	square_polygon = [{:x => 0, :y => 0}, {:x => 1, :y => 0}, {:x => 1, :y => 1}, {:x => 0, :y => 1}]
	in_point = {:x => 0.5, :y => 0.5}
	out_point = {:x => -1, :y => -1}
	puts "In bounds: %s" % pip(in_point, square_polygon)
	puts "Out bounds: %s" % pip(out_point, square_polygon)
when 2
	# Non-convex polygon
	square_polygon = [{:x => 0, :y => 0}, {:x => 1, :y => 0}, {:x => 1, :y => 1}, {:x => 0.5, :y => 0.5}, {:x => 0, :y => 1}]
	p1 = {:x => 0.25, :y => 0.5}
	p2 = {:x => 0.5, :y => 0.75}
	p3 = {:x => 0.75, :y => 0.5}
	p4 = {:x => 0.5, :y => 0.25}
	puts "p1 in bounds: %s" % pip(p1, square_polygon)
	puts "p2 in bounds: %s" % pip(p2, square_polygon)
	puts "p3 in bounds: %s" % pip(p3, square_polygon)
	puts "p4 in bounds: %s" % pip(p4, square_polygon)
end
